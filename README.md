# NEO IOT CLI

## Backlog

* [x] List of datamodel
* [x] Create datamodel
* [x] Read datamodel
* [x] Update datamodel
* [x] Delete datamodel
* [x] CRUD Domain
* [x] CRUD Subject
* [x] CRUD Device
* [x] CRUD Property
* [x] Local repo
* [ ] test with submodule

## poc for "(code)* " 

* [x] use git for data
* [x] use git for reports
* [x] use git for artifacts
* [ ] use git for documents
* [ ] use git for presentation
* [ ] use git for process
* [ ] use git for ideas
* [x] use git for test
* [x] use git for tickets/requirements
* [ ] use git for interface
* [ ] use git for ...


## dog fooding

* [ ] add version to commandline using the git ticketing system
* 


## feedback on the ticketing system in git

* [ ] use editor like swagger editor
* [ ] use view like swagger viewer
* [ ] sensible default
* [ ] DSL for tickets
* [ ] validators for tickets
* 

## related ideas
* [ ] data driven pipelines
* [ ] gitlab as production backend
* [ ] plugion for gitlab for viewers and editors
* [ ] apply "inventing on principle" realtime feedback
* [ ] generate code from swagger
* [ ] pipeline for git repor a la mde